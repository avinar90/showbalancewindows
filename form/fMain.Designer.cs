﻿namespace ITK
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolUrls = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lData = new System.Windows.Forms.Label();
            this.lDayWeek = new System.Windows.Forms.Label();
            this.lTime = new System.Windows.Forms.Label();
            this.lBalance = new System.Windows.Forms.Label();
            this.lUID = new System.Windows.Forms.Label();
            this.pWeatherPic = new System.Windows.Forms.Panel();
            this.lweather = new System.Windows.Forms.Label();
            this.lBalanceNote = new System.Windows.Forms.Label();
            this.lUIDNote = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pWeather = new System.Windows.Forms.Panel();
            this.separator1 = new System.Windows.Forms.Panel();
            this.pDate = new System.Windows.Forms.Panel();
            this.separator2 = new System.Windows.Forms.Panel();
            this.pInfo = new System.Windows.Forms.Panel();
            this.lRubl = new System.Windows.Forms.Label();
            this.separator3 = new System.Windows.Forms.Panel();
            this.pButton = new System.Windows.Forms.Panel();
            this.pClose = new System.Windows.Forms.PictureBox();
            this.pSetting = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.pWeather.SuspendLayout();
            this.pDate.SuspendLayout();
            this.pInfo.SuspendLayout();
            this.pButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.toolUrls,
            this.настройкиToolStripMenuItem,
            this.CloseToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(179, 108);
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Image = global::ITK.Properties.Resources.open_file_40455;
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // toolUrls
            // 
            this.toolUrls.BackColor = System.Drawing.Color.White;
            this.toolUrls.Image = global::ITK.Properties.Resources.videoClub;
            this.toolUrls.Name = "toolUrls";
            this.toolUrls.Size = new System.Drawing.Size(178, 26);
            this.toolUrls.Text = "Полезные ссылки";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Image = global::ITK.Properties.Resources.settings11;
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
            // 
            // CloseToolStripMenuItem
            // 
            this.CloseToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.CloseToolStripMenuItem.Image = global::ITK.Properties.Resources.button_cancel;
            this.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem";
            this.CloseToolStripMenuItem.ShowShortcutKeys = false;
            this.CloseToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.CloseToolStripMenuItem.Text = "Выход";
            this.CloseToolStripMenuItem.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // lData
            // 
            this.lData.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lData.ForeColor = System.Drawing.Color.White;
            this.lData.Location = new System.Drawing.Point(14, 36);
            this.lData.Name = "lData";
            this.lData.Size = new System.Drawing.Size(131, 20);
            this.lData.TabIndex = 8;
            this.lData.Text = "Дата";
            this.lData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lData.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lData.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lData.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lDayWeek
            // 
            this.lDayWeek.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lDayWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lDayWeek.ForeColor = System.Drawing.Color.White;
            this.lDayWeek.Location = new System.Drawing.Point(14, 9);
            this.lDayWeek.Name = "lDayWeek";
            this.lDayWeek.Size = new System.Drawing.Size(131, 20);
            this.lDayWeek.TabIndex = 9;
            this.lDayWeek.Text = "ДЕНЬ";
            this.lDayWeek.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lDayWeek.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lDayWeek.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lDayWeek.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lTime
            // 
            this.lTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lTime.ForeColor = System.Drawing.Color.White;
            this.lTime.Location = new System.Drawing.Point(144, 6);
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(113, 55);
            this.lTime.TabIndex = 10;
            this.lTime.Text = "00:00";
            this.lTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lTime.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lTime.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lTime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lBalance
            // 
            this.lBalance.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lBalance.ForeColor = System.Drawing.Color.White;
            this.lBalance.Location = new System.Drawing.Point(171, 9);
            this.lBalance.Name = "lBalance";
            this.lBalance.Size = new System.Drawing.Size(65, 20);
            this.lBalance.TabIndex = 11;
            this.lBalance.Text = "label4";
            this.lBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lBalance.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lBalance.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lBalance.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lUID
            // 
            this.lUID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lUID.ForeColor = System.Drawing.Color.White;
            this.lUID.Location = new System.Drawing.Point(188, 36);
            this.lUID.Name = "lUID";
            this.lUID.Size = new System.Drawing.Size(65, 20);
            this.lUID.TabIndex = 12;
            this.lUID.Text = "label5";
            this.lUID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lUID.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lUID.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lUID.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // pWeatherPic
            // 
            this.pWeatherPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pWeatherPic.Dock = System.Windows.Forms.DockStyle.Left;
            this.pWeatherPic.Location = new System.Drawing.Point(0, 0);
            this.pWeatherPic.Name = "pWeatherPic";
            this.pWeatherPic.Size = new System.Drawing.Size(93, 66);
            this.pWeatherPic.TabIndex = 13;
            this.pWeatherPic.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.pWeatherPic.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.pWeatherPic.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lweather
            // 
            this.lweather.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lweather.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lweather.ForeColor = System.Drawing.Color.White;
            this.lweather.Location = new System.Drawing.Point(93, 13);
            this.lweather.Margin = new System.Windows.Forms.Padding(0);
            this.lweather.Name = "lweather";
            this.lweather.Size = new System.Drawing.Size(115, 46);
            this.lweather.TabIndex = 16;
            this.lweather.Text = "06";
            this.lweather.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lweather.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lweather.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lweather.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lBalanceNote
            // 
            this.lBalanceNote.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lBalanceNote.AutoSize = true;
            this.lBalanceNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lBalanceNote.ForeColor = System.Drawing.Color.White;
            this.lBalanceNote.Location = new System.Drawing.Point(17, 9);
            this.lBalanceNote.Name = "lBalanceNote";
            this.lBalanceNote.Size = new System.Drawing.Size(104, 20);
            this.lBalanceNote.TabIndex = 14;
            this.lBalanceNote.Text = "Ваш баланс:";
            this.lBalanceNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lBalanceNote.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lBalanceNote.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lBalanceNote.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lUIDNote
            // 
            this.lUIDNote.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lUIDNote.AutoSize = true;
            this.lUIDNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lUIDNote.ForeColor = System.Drawing.Color.White;
            this.lUIDNote.Location = new System.Drawing.Point(17, 36);
            this.lUIDNote.Name = "lUIDNote";
            this.lUIDNote.Size = new System.Drawing.Size(195, 20);
            this.lUIDNote.TabIndex = 15;
            this.lUIDNote.Text = "Ваш уникальный номер: ";
            this.lUIDNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lUIDNote.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.lUIDNote.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.lUIDNote.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Interval = 30000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pWeather
            // 
            this.pWeather.AllowDrop = true;
            this.pWeather.Controls.Add(this.pWeatherPic);
            this.pWeather.Controls.Add(this.separator1);
            this.pWeather.Controls.Add(this.lweather);
            this.pWeather.Location = new System.Drawing.Point(0, 0);
            this.pWeather.Name = "pWeather";
            this.pWeather.Size = new System.Drawing.Size(211, 66);
            this.pWeather.TabIndex = 17;
            this.pWeather.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.pWeather.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.pWeather.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // separator1
            // 
            this.separator1.BackColor = System.Drawing.Color.White;
            this.separator1.Location = new System.Drawing.Point(210, 4);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(1, 58);
            this.separator1.TabIndex = 22;
            // 
            // pDate
            // 
            this.pDate.AllowDrop = true;
            this.pDate.Controls.Add(this.separator2);
            this.pDate.Controls.Add(this.lDayWeek);
            this.pDate.Controls.Add(this.lData);
            this.pDate.Controls.Add(this.lTime);
            this.pDate.Location = new System.Drawing.Point(248, 0);
            this.pDate.Name = "pDate";
            this.pDate.Size = new System.Drawing.Size(260, 66);
            this.pDate.TabIndex = 18;
            this.pDate.Visible = false;
            this.pDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.pDate.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.pDate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // separator2
            // 
            this.separator2.BackColor = System.Drawing.Color.White;
            this.separator2.Location = new System.Drawing.Point(259, 4);
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(1, 58);
            this.separator2.TabIndex = 23;
            // 
            // pInfo
            // 
            this.pInfo.AllowDrop = true;
            this.pInfo.AutoScroll = true;
            this.pInfo.AutoSize = true;
            this.pInfo.Controls.Add(this.lRubl);
            this.pInfo.Controls.Add(this.separator3);
            this.pInfo.Controls.Add(this.lBalanceNote);
            this.pInfo.Controls.Add(this.lUIDNote);
            this.pInfo.Controls.Add(this.lBalance);
            this.pInfo.Controls.Add(this.lUID);
            this.pInfo.Location = new System.Drawing.Point(514, 0);
            this.pInfo.Name = "pInfo";
            this.pInfo.Size = new System.Drawing.Size(267, 66);
            this.pInfo.TabIndex = 19;
            this.pInfo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.pInfo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.pInfo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            // 
            // lRubl
            // 
            this.lRubl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lRubl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lRubl.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lRubl.Location = new System.Drawing.Point(233, 10);
            this.lRubl.Name = "lRubl";
            this.lRubl.Size = new System.Drawing.Size(20, 20);
            this.lRubl.TabIndex = 25;
            this.lRubl.Text = "i";
            this.lRubl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // separator3
            // 
            this.separator3.BackColor = System.Drawing.Color.White;
            this.separator3.Location = new System.Drawing.Point(263, 4);
            this.separator3.Name = "separator3";
            this.separator3.Size = new System.Drawing.Size(1, 58);
            this.separator3.TabIndex = 24;
            // 
            // pButton
            // 
            this.pButton.Controls.Add(this.pClose);
            this.pButton.Controls.Add(this.pSetting);
            this.pButton.Location = new System.Drawing.Point(787, 0);
            this.pButton.Name = "pButton";
            this.pButton.Size = new System.Drawing.Size(23, 66);
            this.pButton.TabIndex = 20;
            // 
            // pClose
            // 
            this.pClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pClose.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pClose.ErrorImage")));
            this.pClose.Image = global::ITK.Properties.Resources.close_white_96x961;
            this.pClose.InitialImage = ((System.Drawing.Image)(resources.GetObject("pClose.InitialImage")));
            this.pClose.Location = new System.Drawing.Point(1, 4);
            this.pClose.Name = "pClose";
            this.pClose.Size = new System.Drawing.Size(19, 29);
            this.pClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pClose.TabIndex = 2;
            this.pClose.TabStop = false;
            this.pClose.Click += new System.EventHandler(this.pClose_Click);
            // 
            // pSetting
            // 
            this.pSetting.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pSetting.Image = global::ITK.Properties.Resources.settings;
            this.pSetting.Location = new System.Drawing.Point(2, 36);
            this.pSetting.Name = "pSetting";
            this.pSetting.Size = new System.Drawing.Size(17, 23);
            this.pSetting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pSetting.TabIndex = 3;
            this.pSetting.TabStop = false;
            this.pSetting.Click += new System.EventHandler(this.pSetting_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 2500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(181, 26);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(3F, 6F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(49)))), ((int)(((byte)(115)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(832, 110);
            this.Controls.Add(this.pWeather);
            this.Controls.Add(this.pButton);
            this.Controls.Add(this.pInfo);
            this.Controls.Add(this.pDate);
            this.Font = new System.Drawing.Font("Times New Roman", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Илим-Телеком";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fMain_FormClosed);
            this.Load += new System.EventHandler(this.fMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fMain_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tMain_MouseUp);
            this.Resize += new System.EventHandler(this.tMain_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.pWeather.ResumeLayout(false);
            this.pDate.ResumeLayout(false);
            this.pInfo.ResumeLayout(false);
            this.pInfo.PerformLayout();
            this.pButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pSetting)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.PictureBox pClose;
        private System.Windows.Forms.PictureBox pSetting;
        private System.Windows.Forms.Label lData;
        private System.Windows.Forms.Label lDayWeek;
        private System.Windows.Forms.Label lTime;
        private System.Windows.Forms.Label lBalance;
        private System.Windows.Forms.Label lUID;
        private System.Windows.Forms.Panel pWeatherPic;
        private System.Windows.Forms.Label lBalanceNote;
        private System.Windows.Forms.Label lUIDNote;
        private System.Windows.Forms.Label lweather;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pWeather;
        private System.Windows.Forms.Panel pDate;
        private System.Windows.Forms.Panel pInfo;
        private System.Windows.Forms.Panel pButton;
        private System.Windows.Forms.Panel separator1;
        private System.Windows.Forms.Panel separator2;
        private System.Windows.Forms.Panel separator3;
        private System.Windows.Forms.Label lRubl;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolUrls;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
    }
}

