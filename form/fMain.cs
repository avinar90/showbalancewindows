﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.Timers;
using System.Drawing.Text;
using System.Diagnostics;
using MySql.Data.MySqlClient;
using System.Threading;

namespace ITK
{
    public partial class fMain : Form
    {
        PrivateFontCollection font;
        private Int32 tmpX;
        private Int32 tmpY;
        private bool flMove = false;
        private fSetting settingForm;
        public string balance;
        public string uid;
        public string url;
        public int znak;
        public string bufferWeatherUI=("- °C");
        public string bufferWeatherS=("- °C");
        public Bitmap bufferWeatherPicUI;
        public Bitmap bufferWeatherPicS;

        public List<string> list = new List<string>();

        public int i;
        public string url_camer;
        public string url_private;
        public string url_vclub;
        public string url_iptv;

        public string textError;
        public string textErrorMessage;
        string Date = DateTime.Now.ToString("dd/MM/yy H:mm");
        string IP;

        log applog = new log();

        public fMain()
        {
            try
            {
                InitializeComponent();   
                loadFonts();
                setFonts();
                settingForm = new fSetting(this);
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }

        }

        public void fMain_Load(object sender, EventArgs e)
        {
            try
            {
                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[START PROGRAM]");
                lRubl.Text = "\u20BD";
                notifyIcon1.Visible = true;
                clearForm();
                setMainSetting();
                setSizeWidget();
                getWeather();
                showDateTime();
                getDataBilling();

                settingUpnp();
                loadUpnp();
                
                timer1.Enabled = true;
                timer2.Enabled = true;
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void setMainSetting()
        {
            try
            {                
                getUrls();
                getIP();


                /// TEST    /// TEST        /// TEST    /// TEST       /// TEST    /// TEST      /// TEST    /// TEST      /// TEST    /// TEST      /// TEST    /// TEST      /// TEST    /// TEST 
                //test();
                /// TEST    /// TEST        /// TEST    /// TEST       /// TEST    /// TEST      /// TEST    /// TEST      /// TEST    /// TEST      /// TEST    /// TEST      /// TEST    /// TEST

                //всплывающий
                //при наводе      
                if (Properties.Settings.Default.CheckedTraySave == true)
                {
                    formHide();
                    //Console.WriteLine("setMainSettings");
                }
                windowPosition();
                settingForm.Visible = false;
                //timer1.Enabled = true;
                pWeather.Visible = Properties.Settings.Default.CheckedWeatherSave;
                pDate.Visible = Properties.Settings.Default.CheckedDateSave;
                pInfo.Visible = Properties.Settings.Default.CheckedInfoSave;
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void getIP()
        {
            try
            {
                String host = System.Net.Dns.GetHostName();
                int b = 0;
                for (int index = 0; index < Dns.GetHostByName(host).AddressList.Length; index++)
                {
                    b = index;
                }
                IP = Convert.ToString(Dns.GetHostByName(host).AddressList[b]);
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }

        }

       
        public void test()
        {
            int k = 0;
            try
            {
                int j = 1/k;
            }
            catch(Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }
        
              
        public void error()
        {
            try
            {
                MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();
                conn_string.Server = "ta.uilim.ru";
                conn_string.UserID = "Loger";
                conn_string.Password = "Log01KJU";
                conn_string.Database = "yii2basic";
                conn_string.Port = 3306;
                conn_string.CharacterSet = "utf8";

                MySqlConnection myConnection = new MySqlConnection(conn_string.ConnectionString);

                string insert = ("INSERT Into Log (Date,IP,Message,Exception) VALUES ( '" + Date + "' ,'" + IP + "','" + textError + "','" + textErrorMessage + "')");
                myConnection.Open();
                MySqlCommand cmd = new MySqlCommand(insert, myConnection);
                cmd.ExecuteNonQuery();
                myConnection.Close();
                textError = "";
                textErrorMessage = "";
            }
            catch
            {
                //MessageBox.Show(ex.ToString());
                //MessageBox.Show("Извините. Проблема с соединением. Перезапустите приложение.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //this.Close();
            }

        }

        public void windowPosition()
        {
            try
            {
                if (Properties.Settings.Default.FirstRun == true)
                {
                    this.Location = new Point((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                              (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                    Properties.Settings.Default.ComboWeatherID = 0;
                    Properties.Settings.Default.ComboWeatherIcon = (@Application.StartupPath + "/pictures/icons0");
                    
                    //Properties.Settings.Default.ColorForm = "46; 49; 115";
                    
                    Properties.Settings.Default.FirstRun = false;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    this.Location = Properties.Settings.Default.Location;
                }
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void loadFonts()
        {
            try
            {
                this.font = new PrivateFontCollection();
                this.font.AddFontFile(@Application.StartupPath + "/font/pt-sans.ttf");   //1         
                this.font.AddFontFile(@Application.StartupPath + "/font/weather.ttf");//2
                this.font.AddFontFile(@Application.StartupPath + "/font/PTM75F.ttf");//0
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void setFonts()
        {
            try
            {
                string X = ("93"); lweather.Location.X.ToString(X); string Y = ("13"); lweather.Location.X.ToString(Y);

                lweather.Font = new Font(font.Families[2], 20, FontStyle.Bold);
                lTime.Font = new Font(font.Families[1], 26, FontStyle.Bold);
                lRubl.Font = new Font(font.Families[0], 12, FontStyle.Bold);
                lBalance.Font = new Font(font.Families[1], 12, FontStyle.Bold);
                lUID.Font = new Font(font.Families[1], 12, FontStyle.Bold);
                lDayWeek.Font = new Font(font.Families[1], 12);
                lData.Font = new Font(font.Families[1], 12);
                lBalanceNote.Font = new Font(font.Families[1], 12);
                lUIDNote.Font = new Font(font.Families[1], 12);
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                fMainOpen();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void tMain_Resize(object sender, EventArgs e)
        {
            try
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    this.Hide();
                    //notifyIcon1.Visible = true;
                }
                else if (FormWindowState.Normal == this.WindowState)
                {
                    //notifyIcon1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void fMainOpen()
        {
            try
            {
                //notifyIcon1.Visible = false;
                this.Show();
                WindowState = FormWindowState.Normal;
                this.Size = new Size(0, 0);
                setSizeWidget();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    fMainOpen();
                }
                else if (FormWindowState.Normal == this.WindowState)
                {
                    this.Location = new Point((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                              (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                    fMainOpen();
                }
                
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void clearForm()
        {
            try
            {
                lData.Text = " "; lDayWeek.Text = " ";
                lTime.Text = " "; lBalance.Text = " ";
                lUID.Text = " "; lweather.Text = " ";
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void getWeather()
        {
            try
            {
                //string url = "http://91.219.137.30/api/weather";
                //HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                //HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //string response;
                //using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                //{
                //    response = streamReader.ReadToEnd();
                //}

                //OpenWeather.OpenWeather oW = JsonConvert.DeserializeObject<OpenWeather.OpenWeather>(response);

        
                string X = ("93"); lweather.Location.X.ToString(X);
                string Y = ("13"); lweather.Location.X.ToString(Y);
                
                //MessageBox.Show(bufferWeather);
                try
                {
                    lweather.Font = new Font(font.Families[2], 20, FontStyle.Bold);
                    string url = "https://ta.uilim.ru/api/weather";
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    string response;
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        response = streamReader.ReadToEnd();
                    }

                    var apiResponse = (ApiResponse)JsonConvert.DeserializeObject(response, typeof(ApiResponse));
                    bufferWeatherUI = (apiResponse.Weather[0].temp.ToString("0") + " °C");
                    bufferWeatherS = (apiResponse.Weather[1].temp.ToString("0") + " °C");
                    bufferWeatherPicUI=apiResponse.Weather[0].Icon;
                    bufferWeatherPicS=apiResponse.Weather[1].Icon;

                    if (Properties.Settings.Default.CheckedWeatherCity == 0)
                    {
                        pWeatherPic.BackgroundImage = apiResponse.Weather[0].Icon;
                        lweather.Text = (apiResponse.Weather[0].temp.ToString("0") + " °C");
                    }
                    else
                    {
                        pWeatherPic.BackgroundImage = apiResponse.Weather[1].Icon;
                        lweather.Text = (apiResponse.Weather[1].temp.ToString("0") + " °C");
                    }
                }
                catch
                {
                    StreamReader streamReader = new StreamReader(Application.StartupPath + "\\information");
                    string strI = streamReader.ReadLine();
                    string strSearch = strI.Replace("uid = ", "");
                    streamReader.Close();
                    if (strSearch == "0")
                    {
                        string x = ("13"); lweather.Location.X.ToString(x);
                        string y = ("13"); lweather.Location.X.ToString(y);
                        lweather.Font = new Font(font.Families[2], 8, FontStyle.Bold);
                        lweather.Text = "Проверьте соединение";
                    }
                    else
                    {
                        lweather.Font = new Font(font.Families[2], 20, FontStyle.Bold);
                        if (Properties.Settings.Default.CheckedWeatherCity == 0)
                        {
                            pWeatherPic.BackgroundImage = bufferWeatherPicUI;
                            lweather.Text = bufferWeatherUI;
                        }
                        else
                        {
                            pWeatherPic.BackgroundImage = bufferWeatherPicS;
                            lweather.Text = bufferWeatherS;
                        }
                    }       
                }
            }
            catch (Exception ex)
            {
                string X = ("93"); lweather.Location.X.ToString(X);
                string Y = ("13"); lweather.Location.X.ToString(Y);
                lweather.Visible = true;

                lweather.Font = new Font(font.Families[2], 6, FontStyle.Bold);
                lweather.Text = "Соединение";
                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[PROBLEM CONNECT VISIBLE WEATHER]");                
                
                textError = ("Нет Соединения с сайтом, function getWeather");
                textErrorMessage = ("Нет Соединения с API Weather");                                   

                error();            
            }
        }

        public void showDateTime()
        {
            try
            {
                lData.Text = DateTime.Now.ToString("dd MMMM yyyy");
                lDayWeek.Text = DateTime.Now.ToString("dddd");
                lTime.Text = DateTime.Now.ToString("HH:mm");
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        //public void API()
        //{
        //    string url = "https://c.uilim.ru/api/info";
        //    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
        //    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //    string response;
        //    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
        //    {
        //        response = streamReader.ReadToEnd();
        //    }

        //    InfoResponse infoResponse = JsonConvert.DeserializeObject<InfoResponse>(response);
        //    MessageBox.Show(infoResponse.ipService.cash.ToString());
        //}

        public void getDataBilling()
        {
            try
            {
                string path = (@Application.StartupPath + "\\information");
                try
                {
                    string url = "https://c.uilim.ru/api/info";
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    string response;
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        response = streamReader.ReadToEnd();
                    }

                    InfoResponse infoResponse = JsonConvert.DeserializeObject<InfoResponse>(response);

                    
                    balance = infoResponse.ipService.cash.ToString();
                    uid = infoResponse.ipService.uid.ToString();

                    StreamWriter streamWriter = new StreamWriter(path);
                    streamWriter.Flush();
                    streamWriter.WriteLine("uid = " + uid);
                    streamWriter.WriteLine("balance = " + balance);
                    streamWriter.Close();

                    showBillingData();
                }
                catch
                {
                    StreamReader streamReader = new StreamReader(path);
                    string text = streamReader.ReadLine();
                    string text2 = streamReader.ReadLine();
                    string strokaUid = text.Replace("uid = ", "");
                    string strokaBalance = text2.Replace("balance = ", "");
                    streamReader.Close();


                    balance = strokaBalance;
                    uid = strokaUid;
                    showBillingData();
                }
                
            }
            catch (Exception ex)
            {
                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[PROBLEM CONNECT VISIBLE BALANCE]");

                lBalance.Font = new Font(font.Families[1], 10, FontStyle.Bold);
                lUID.Font = new Font(font.Families[1], 10, FontStyle.Bold);
                lBalance.Text = "Ошибка";
                lUID.Text = "Ошибка";

                textError = ("Нет Соединения https://c.uilim.ru/api/info, function getDataBilling");
                textErrorMessage = ("Нет Соединения с API c.uilim.ru/api/info");
                error();
            }
        }

        public void showBillingData()
        {
            try
            {
                lBalance.Text = balance;
                lUID.Text = uid;
                notifyIcon1.Text = "Илим-Телеком, баланс " + balance + " рублей";
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void tMain_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                tmpX = Cursor.Position.X;
                tmpY = Cursor.Position.Y;
                flMove = true;
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void tMain_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                flMove = false;
                if (e.Button==MouseButtons.Right)
                {
                    contextMenuStrip2.Show(MousePosition, ToolStripDropDownDirection.Right);                   
                }                   
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void fMain_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (flMove)
                {
                    this.Left = this.Left + (Cursor.Position.X - tmpX);
                    this.Top = this.Top + (Cursor.Position.Y - tmpY);

                    tmpX = Cursor.Position.X;
                    tmpY = Cursor.Position.Y;
                }
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                getWeather();
                getDataBilling();
                getUrls();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                showDateTime();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                setSizeWidget();
                settingForm.Show();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public bool pDateClose
        {
            get
            {
                return this.pDate.Visible;
            }
            set
            {
                this.pDate.Visible = value;
            }
        }

        public bool pWeatherClose
        {
            get
            {
                return this.pWeather.Visible;
            }
            set
            {
                this.pWeather.Visible = value;
            }
        }

        public bool pInfoClose
        {
            get
            {
                return this.pInfo.Visible;
            }
            set
            {
                this.pInfo.Visible = value;
            }
        }


        public void pClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.CheckedCloseSave == true)
                {
                    windowSavePosition();
                    this.Close();
                }
                else
                {
                    formHide();
                }
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void formHide()
        {
            try
            {

                WindowState = FormWindowState.Minimized;
                //notifyIcon1.Visible = true;
                //Console.WriteLine("formHide");
                notifyIcon1.BalloonTipTitle = "Илим-Телеком";
                notifyIcon1.BalloonTipText = "Приложение свернуто";
                notifyIcon1.ShowBalloonTip(1000); //работает????    
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void pSetting_Click(object sender, EventArgs e)
        {
            try
            {
                settingForm.Show();//добавить условие на закрытие при откритии
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void setSizeWidget()
        {
            try
            {
                this.BackColor = Properties.Settings.Default.ColorForm;
                separator1.BackColor = Properties.Settings.Default.ColorText;
                separator2.BackColor = Properties.Settings.Default.ColorText;
                separator3.BackColor = Properties.Settings.Default.ColorText;
                lweather.ForeColor= Properties.Settings.Default.ColorText;
                lDayWeek.ForeColor = Properties.Settings.Default.ColorText;
                lData.ForeColor = Properties.Settings.Default.ColorText;
                lTime.ForeColor = Properties.Settings.Default.ColorText;
                lBalance.ForeColor = Properties.Settings.Default.ColorText;
                lBalanceNote.ForeColor = Properties.Settings.Default.ColorText;
                lUIDNote.ForeColor = Properties.Settings.Default.ColorText;
                lUID.ForeColor = Properties.Settings.Default.ColorText;
                lRubl.ForeColor = Properties.Settings.Default.ColorText;    




                if ((pWeather.Visible == true) && (pDate.Visible == true) && (pInfo.Visible == true) && (pButton.Visible == true))
                {
                    pWeather.Location = new Point(0, 0);
                    pDate.Location = new Point(212, 0);
                    pInfo.Location = new Point(472, 0);
                    pButton.Location = new Point(741, 0);
                }
                if ((pWeather.Visible == false) && (pDate.Visible == false) && (pInfo.Visible == true) && (pButton.Visible == true))
                {
                    pInfo.Location = new Point(0, 0);
                    pButton.Location = new Point(269, 0);
                }
                if ((pWeather.Visible == false) && (pDate.Visible == true) && (pInfo.Visible == true) && (pButton.Visible == true))
                {
                    pDate.Location = new Point(0, 0);
                    pInfo.Location = new Point(261, 0);
                    pButton.Location = new Point(530, 0);
                }
                if ((pWeather.Visible == true) && (pDate.Visible == false) && (pInfo.Visible == true) && (pButton.Visible == true))
                {
                    pWeather.Location = new Point(0, 0);
                    pInfo.Location = new Point(212, 0);
                    pButton.Location = new Point(480, 0);
                }
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                windowSavePosition();
                this.Close();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        public void windowSavePosition()
        {
            try
            {
                if (this.WindowState == FormWindowState.Normal)
                {
                    Properties.Settings.Default.Location = this.Location;
                }
                else
                {
                    Properties.Settings.Default.Location = this.RestoreBounds.Location;
                }
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }


        public Boolean getUrls()
        {
            try
            {
                if (toolUrls.DropDownItems.Count == 0) 
                {
                    string path = (@Application.StartupPath + "/pic/");
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                    string url = "https://ta.uilim.ru/api/links";
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    string response;
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        response = streamReader.ReadToEnd();
                    }

                    var apiResponse = (ApiResponse)JsonConvert.DeserializeObject(response, typeof(ApiResponse));

                    for (i = 0; i < apiResponse.links.Count; i++)
                    {


                        toolUrls.DropDownItems.Add(apiResponse.links[i].name);
                        toolUrls.DropDownItems[i].Name = apiResponse.links[i].name;

                        // toolUrls.DropDownItems[i].Image= apiResponse.information.links[i].icon;

                        list.Add(Convert.ToString(apiResponse.links[i].url));
                        toolUrls.DropDownItems[i].Click += new EventHandler(toolUrls_Click);


                        byte[] buffer = Convert.FromBase64String(apiResponse.links[i].icon);
                        File.WriteAllBytes(@Application.StartupPath + "/pic/pic_" + i + ".jpg", buffer);
                        toolUrls.DropDownItems[i].Image = Image.FromFile(@Application.StartupPath + "/pic/pic_" + i + ".jpg");

                        contextMenuStrip2.Items.Add(apiResponse.links[i].name);
                        contextMenuStrip2.Items[i].Name = apiResponse.links[i].name;
                        contextMenuStrip2.Items[i].Click += new EventHandler(toolUrls_Click);
                        contextMenuStrip2.Items[i].Image = Image.FromFile(@Application.StartupPath + "/pic/pic_" + i + ".jpg");
                    }
                    toolUrls.Visible = true;
                }
                return true;
            }
            catch (Exception ex)
            {
                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[PROBLEM VISIBLE ICON]");
                toolUrls.Visible = false;
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();

                return false;
                                                            
            }
        }

              
        private void toolUrls_Click(object sender, EventArgs e)
        {
            try
            {
                ToolStripDropDownItem item = sender as ToolStripDropDownItem;
                int Index = ((ToolStripItem)sender).Owner.Items.IndexOf((ToolStripItem)sender);
                url = list[Index];
                Process.Start(url);
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }

        private void fMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {

                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[EXIT PROGRAM]");
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }
         
        public void loadUpnp()
        {
            try
            {                
                if (Properties.Settings.Default.CheckedUpnpSave == true)
                {
                    znak = 0;
                    foreach (Process pr in Process.GetProcessesByName("xupnpd"))
                    {
                        znak = 1;
                    }
                    if (znak==0)
                    {
                        new Process()
                        {
                            StartInfo = {
                                FileName = (Application.StartupPath + "/upnp/xupnpd.exe"),
                                WorkingDirectory = Application.StartupPath,
                                CreateNoWindow = true
                             }
                        }.Start();                        
                    }
                    znak = 0;
                }
                else
                {

                }                
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        } 
        
        public void settingUpnp()
        {
            try
            {
                string path = (@Application.StartupPath + "\\upnp\\xupnpd_uuid.lua");
                Guid guid = Guid.NewGuid();
                IPHostEntry hostByName = Dns.GetHostByName(Dns.GetHostName());
               

                StreamWriter streamWriter = new StreamWriter(path);
                streamWriter.Flush();
                streamWriter.WriteLine("uuid = \"" + guid.ToString() + "\"");
                streamWriter.WriteLine("hostip = \"" + hostByName.AddressList[0].ToString() + "\"");
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                textError = ex.ToString();
                textErrorMessage = ex.Message.ToString();
                error();
            }
        }
        
    }
}
