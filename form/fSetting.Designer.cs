﻿namespace ITK
{
    partial class fSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fSetting));
            this.checkRun = new System.Windows.Forms.CheckBox();
            this.checkDate = new System.Windows.Forms.CheckBox();
            this.checkWeather = new System.Windows.Forms.CheckBox();
            this.checkInfo = new System.Windows.Forms.CheckBox();
            this.checkTray = new System.Windows.Forms.CheckBox();
            this.checkClose = new System.Windows.Forms.CheckBox();
            this.comboWeather = new System.Windows.Forms.ComboBox();
            this.gRun = new System.Windows.Forms.GroupBox();
            this.gForms = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lVersion = new System.Windows.Forms.Label();
            this.bUpdate = new System.Windows.Forms.Button();
            this.gSupport = new System.Windows.Forms.GroupBox();
            this.checkUpnp = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBoxColor = new System.Windows.Forms.GroupBox();
            this.buttonColorForm = new System.Windows.Forms.Button();
            this.buttonDropping = new System.Windows.Forms.Button();
            this.buttonColorText = new System.Windows.Forms.Button();
            this.labelColorText = new System.Windows.Forms.Label();
            this.labelColorForm = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBoxWeather = new System.Windows.Forms.GroupBox();
            this.comboBoxWeather = new System.Windows.Forms.ComboBox();
            this.colorDialogForm = new System.Windows.Forms.ColorDialog();
            this.gRun.SuspendLayout();
            this.gForms.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gSupport.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBoxColor.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBoxWeather.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkRun
            // 
            this.checkRun.AutoSize = true;
            this.checkRun.Location = new System.Drawing.Point(6, 19);
            this.checkRun.Name = "checkRun";
            this.checkRun.Size = new System.Drawing.Size(134, 17);
            this.checkRun.TabIndex = 0;
            this.checkRun.Text = "Запустить с Windows";
            this.checkRun.UseVisualStyleBackColor = true;
            this.checkRun.CheckedChanged += new System.EventHandler(this.checkRun_CheckedChanged);
            // 
            // checkDate
            // 
            this.checkDate.AutoSize = true;
            this.checkDate.Location = new System.Drawing.Point(6, 42);
            this.checkDate.Name = "checkDate";
            this.checkDate.Size = new System.Drawing.Size(158, 17);
            this.checkDate.TabIndex = 1;
            this.checkDate.Text = "Показывать дату и время";
            this.checkDate.UseVisualStyleBackColor = true;
            this.checkDate.CheckedChanged += new System.EventHandler(this.checkDate_CheckedChanged);
            // 
            // checkWeather
            // 
            this.checkWeather.AutoSize = true;
            this.checkWeather.Location = new System.Drawing.Point(6, 19);
            this.checkWeather.Name = "checkWeather";
            this.checkWeather.Size = new System.Drawing.Size(125, 17);
            this.checkWeather.TabIndex = 4;
            this.checkWeather.Text = "Отображать погоду";
            this.checkWeather.UseVisualStyleBackColor = true;
            this.checkWeather.CheckedChanged += new System.EventHandler(this.checkWeather_CheckedChanged);
            // 
            // checkInfo
            // 
            this.checkInfo.AutoSize = true;
            this.checkInfo.Location = new System.Drawing.Point(6, 65);
            this.checkInfo.Name = "checkInfo";
            this.checkInfo.Size = new System.Drawing.Size(211, 17);
            this.checkInfo.TabIndex = 5;
            this.checkInfo.Text = "Отображать информацию о балансе";
            this.checkInfo.UseVisualStyleBackColor = true;
            this.checkInfo.CheckedChanged += new System.EventHandler(this.checkInfo_CheckedChanged);
            // 
            // checkTray
            // 
            this.checkTray.AutoSize = true;
            this.checkTray.Location = new System.Drawing.Point(6, 42);
            this.checkTray.Name = "checkTray";
            this.checkTray.Size = new System.Drawing.Size(192, 17);
            this.checkTray.TabIndex = 6;
            this.checkTray.Text = "При запуске сворачивать в трей";
            this.checkTray.UseVisualStyleBackColor = true;
            this.checkTray.CheckedChanged += new System.EventHandler(this.checkTray_CheckedChanged);
            // 
            // checkClose
            // 
            this.checkClose.AutoSize = true;
            this.checkClose.Location = new System.Drawing.Point(6, 65);
            this.checkClose.Name = "checkClose";
            this.checkClose.Size = new System.Drawing.Size(141, 17);
            this.checkClose.TabIndex = 7;
            this.checkClose.Text = "Закрывать на крестик";
            this.checkClose.UseVisualStyleBackColor = true;
            this.checkClose.CheckedChanged += new System.EventHandler(this.checkClose_CheckedChanged);
            // 
            // comboWeather
            // 
            this.comboWeather.FormattingEnabled = true;
            this.comboWeather.Items.AddRange(new object[] {
            "Deafault",
            "Android",
            "Vclouds",
            "Voluminous",
            "Drawn"});
            this.comboWeather.Location = new System.Drawing.Point(6, 19);
            this.comboWeather.Name = "comboWeather";
            this.comboWeather.Size = new System.Drawing.Size(212, 21);
            this.comboWeather.TabIndex = 8;
            this.comboWeather.SelectedIndexChanged += new System.EventHandler(this.comboWeather_SelectedIndexChanged);
            // 
            // gRun
            // 
            this.gRun.Controls.Add(this.checkRun);
            this.gRun.Controls.Add(this.checkTray);
            this.gRun.Controls.Add(this.checkClose);
            this.gRun.Location = new System.Drawing.Point(7, 6);
            this.gRun.Name = "gRun";
            this.gRun.Size = new System.Drawing.Size(220, 90);
            this.gRun.TabIndex = 9;
            this.gRun.TabStop = false;
            this.gRun.Text = "При запуске";
            // 
            // gForms
            // 
            this.gForms.Controls.Add(this.checkDate);
            this.gForms.Controls.Add(this.checkInfo);
            this.gForms.Controls.Add(this.checkWeather);
            this.gForms.Location = new System.Drawing.Point(7, 102);
            this.gForms.Name = "gForms";
            this.gForms.Size = new System.Drawing.Size(220, 63);
            this.gForms.TabIndex = 10;
            this.gForms.TabStop = false;
            this.gForms.Text = "Отображать";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(1, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(243, 275);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lVersion);
            this.tabPage1.Controls.Add(this.bUpdate);
            this.tabPage1.Controls.Add(this.gSupport);
            this.tabPage1.Controls.Add(this.gRun);
            this.tabPage1.Controls.Add(this.gForms);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(235, 249);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Система";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lVersion
            // 
            this.lVersion.AutoSize = true;
            this.lVersion.Location = new System.Drawing.Point(181, 222);
            this.lVersion.Name = "lVersion";
            this.lVersion.Size = new System.Drawing.Size(46, 13);
            this.lVersion.TabIndex = 13;
            this.lVersion.Text = "v1.1.1.1";
            // 
            // bUpdate
            // 
            this.bUpdate.Location = new System.Drawing.Point(6, 217);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(75, 23);
            this.bUpdate.TabIndex = 12;
            this.bUpdate.Text = "Обновить";
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // gSupport
            // 
            this.gSupport.Controls.Add(this.checkUpnp);
            this.gSupport.Location = new System.Drawing.Point(7, 167);
            this.gSupport.Name = "gSupport";
            this.gSupport.Size = new System.Drawing.Size(220, 44);
            this.gSupport.TabIndex = 11;
            this.gSupport.TabStop = false;
            this.gSupport.Text = "Поддержка";
            // 
            // checkUpnp
            // 
            this.checkUpnp.AutoSize = true;
            this.checkUpnp.Location = new System.Drawing.Point(6, 19);
            this.checkUpnp.Name = "checkUpnp";
            this.checkUpnp.Size = new System.Drawing.Size(122, 17);
            this.checkUpnp.TabIndex = 0;
            this.checkUpnp.Text = "UPnP телевидения";
            this.checkUpnp.UseVisualStyleBackColor = true;
            this.checkUpnp.CheckedChanged += new System.EventHandler(this.checkUpnp_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxColor);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(235, 249);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Тема";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBoxColor
            // 
            this.groupBoxColor.Controls.Add(this.buttonColorForm);
            this.groupBoxColor.Controls.Add(this.buttonDropping);
            this.groupBoxColor.Controls.Add(this.buttonColorText);
            this.groupBoxColor.Controls.Add(this.labelColorText);
            this.groupBoxColor.Controls.Add(this.labelColorForm);
            this.groupBoxColor.Location = new System.Drawing.Point(6, 60);
            this.groupBoxColor.Name = "groupBoxColor";
            this.groupBoxColor.Size = new System.Drawing.Size(222, 114);
            this.groupBoxColor.TabIndex = 18;
            this.groupBoxColor.TabStop = false;
            this.groupBoxColor.Text = "Редактирование формы";
            // 
            // buttonColorForm
            // 
            this.buttonColorForm.Location = new System.Drawing.Point(6, 19);
            this.buttonColorForm.Name = "buttonColorForm";
            this.buttonColorForm.Size = new System.Drawing.Size(179, 23);
            this.buttonColorForm.TabIndex = 13;
            this.buttonColorForm.Text = "Задать цвет формы";
            this.buttonColorForm.UseVisualStyleBackColor = true;
            this.buttonColorForm.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonDropping
            // 
            this.buttonDropping.Location = new System.Drawing.Point(6, 78);
            this.buttonDropping.Name = "buttonDropping";
            this.buttonDropping.Size = new System.Drawing.Size(179, 23);
            this.buttonDropping.TabIndex = 17;
            this.buttonDropping.Text = "Настройки по умолчанию";
            this.buttonDropping.UseVisualStyleBackColor = true;
            this.buttonDropping.Click += new System.EventHandler(this.buttonDropping_Click);
            // 
            // buttonColorText
            // 
            this.buttonColorText.Location = new System.Drawing.Point(6, 48);
            this.buttonColorText.Name = "buttonColorText";
            this.buttonColorText.Size = new System.Drawing.Size(179, 23);
            this.buttonColorText.TabIndex = 14;
            this.buttonColorText.Text = "Задать цвет текста";
            this.buttonColorText.UseVisualStyleBackColor = true;
            this.buttonColorText.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelColorText
            // 
            this.labelColorText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelColorText.Location = new System.Drawing.Point(191, 48);
            this.labelColorText.Name = "labelColorText";
            this.labelColorText.Size = new System.Drawing.Size(23, 23);
            this.labelColorText.TabIndex = 16;
            this.labelColorText.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelColorForm
            // 
            this.labelColorForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelColorForm.Location = new System.Drawing.Point(191, 19);
            this.labelColorForm.Name = "labelColorForm";
            this.labelColorForm.Size = new System.Drawing.Size(23, 23);
            this.labelColorForm.TabIndex = 15;
            this.labelColorForm.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboWeather);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(222, 48);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Тема погоды";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBoxWeather);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(235, 249);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Погода";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBoxWeather
            // 
            this.groupBoxWeather.Controls.Add(this.comboBoxWeather);
            this.groupBoxWeather.Location = new System.Drawing.Point(4, 4);
            this.groupBoxWeather.Name = "groupBoxWeather";
            this.groupBoxWeather.Size = new System.Drawing.Size(227, 49);
            this.groupBoxWeather.TabIndex = 0;
            this.groupBoxWeather.TabStop = false;
            this.groupBoxWeather.Text = "Выберите свой город";
            // 
            // comboBoxWeather
            // 
            this.comboBoxWeather.FormattingEnabled = true;
            this.comboBoxWeather.Items.AddRange(new object[] {
            "Усть-Илимск",
            "Саянск"});
            this.comboBoxWeather.Location = new System.Drawing.Point(7, 20);
            this.comboBoxWeather.Name = "comboBoxWeather";
            this.comboBoxWeather.Size = new System.Drawing.Size(214, 21);
            this.comboBoxWeather.TabIndex = 0;
            this.comboBoxWeather.SelectedIndexChanged += new System.EventHandler(this.comboBoxWeather_SelectedIndexChanged);
            // 
            // colorDialogForm
            // 
            this.colorDialogForm.AnyColor = true;
            this.colorDialogForm.FullOpen = true;
            // 
            // fSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(248, 368);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Fsetting_FormClosing);
            this.Load += new System.EventHandler(this.Fsetting_Load);
            this.gRun.ResumeLayout(false);
            this.gRun.PerformLayout();
            this.gForms.ResumeLayout(false);
            this.gForms.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gSupport.ResumeLayout(false);
            this.gSupport.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBoxColor.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBoxWeather.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkRun;
        private System.Windows.Forms.CheckBox checkDate;
        private System.Windows.Forms.CheckBox checkWeather;
        private System.Windows.Forms.CheckBox checkInfo;
        private System.Windows.Forms.CheckBox checkTray;
        private System.Windows.Forms.CheckBox checkClose;
        private System.Windows.Forms.ComboBox comboWeather;
        private System.Windows.Forms.GroupBox gRun;
        private System.Windows.Forms.GroupBox gForms;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ColorDialog colorDialogForm;
        private System.Windows.Forms.Button buttonColorForm;
        private System.Windows.Forms.Button buttonColorText;
        private System.Windows.Forms.Label labelColorForm;
        private System.Windows.Forms.Label labelColorText;
        private System.Windows.Forms.Button buttonDropping;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBoxWeather;
        private System.Windows.Forms.ComboBox comboBoxWeather;
        private System.Windows.Forms.GroupBox groupBoxColor;
        private System.Windows.Forms.GroupBox gSupport;
        private System.Windows.Forms.CheckBox checkUpnp;
        private System.Windows.Forms.Label lVersion;
        private System.Windows.Forms.Button bUpdate;
    }
}