﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace ITK
{
    public partial class fSetting : Form
    {
        
        private fMain parentForm;
        log applog = new log();


        public fSetting(fMain parentForm)
        {
            InitializeComponent();
            this.parentForm = parentForm;
            parentForm.setSizeWidget();
        }

        private void Fsetting_Load(object sender, EventArgs e)
        {
            saveSetting(); 
            
        }

        private void saveSetting()
        {
            try
            {
                checkInfo.Visible = false;
                this.checkRun.Checked = Properties.Settings.Default.CheckedRunSave;
                this.checkInfo.Checked = Properties.Settings.Default.CheckedInfoSave;
                this.checkDate.Checked = Properties.Settings.Default.CheckedDateSave;
                this.checkWeather.Checked = Properties.Settings.Default.CheckedWeatherSave;
                this.checkTray.Checked = Properties.Settings.Default.CheckedTraySave;
                this.checkClose.Checked = Properties.Settings.Default.CheckedCloseSave;
                this.checkUpnp.Checked = Properties.Settings.Default.CheckedUpnpSave;

                this.comboWeather.SelectedIndex = Properties.Settings.Default.ComboWeatherID;
                this.comboBoxWeather.SelectedIndex = Properties.Settings.Default.CheckedWeatherCity;
               

                labelColorForm.BackColor = Properties.Settings.Default.ColorForm;
                labelColorText.BackColor = Properties.Settings.Default.ColorText;

                lVersion.Text=("v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            }
            catch (Exception ex)
            {
                parentForm.textError = ex.ToString();
                parentForm.textErrorMessage = ex.Message.ToString();
                parentForm.error();
            }

        }
        
        private void checkRun_CheckedChanged(object sender, EventArgs e)
        {
            string ExePath = Application.ExecutablePath;
            RegistryKey reg;
            string name = "ShowBalance";
            CheckBox checkBox = (CheckBox)sender;
            if (checkBox.Checked == true)
            {
                reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\", true);
                reg.SetValue(name, ExePath);
                reg.Close();
                Properties.Settings.Default.CheckedRunSave = checkRun.Checked;
                Properties.Settings.Default.Save();
            }
            else
            {
                reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\", true);
                reg.DeleteValue(name);//Если файла там нету, ничего не проидойдет(true)
                reg.Close();//сохраняем изменения

                Properties.Settings.Default.CheckedRunSave = checkRun.Checked;
                Properties.Settings.Default.Save();
            }
        }
        
        private void Fsetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }            
        }

        private void checkWeather_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            parentForm.pWeatherClose = checkBox.Checked;          
            Properties.Settings.Default.CheckedWeatherSave = checkWeather.Checked;
            Properties.Settings.Default.Save();
            parentForm.setSizeWidget();
        }

        private void checkInfo_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;            
            parentForm.pInfoClose = checkBox.Checked;
            Properties.Settings.Default.CheckedInfoSave = checkInfo.Checked;
            Properties.Settings.Default.Save();
            parentForm.setSizeWidget();
        }

        private void checkDate_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            parentForm.pDateClose = checkBox.Checked;
            Properties.Settings.Default.CheckedDateSave = checkDate.Checked;
            Properties.Settings.Default.Save();
            parentForm.setSizeWidget();
        }

        private void checkTray_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            Properties.Settings.Default.CheckedTraySave = checkBox.Checked;
            Properties.Settings.Default.Save();
            parentForm.setSizeWidget();
        }

        private void checkClose_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            Properties.Settings.Default.CheckedCloseSave = checkClose.Checked;
            Properties.Settings.Default.Save();
            parentForm.setSizeWidget();
        }

        private void checkUpnp_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            Properties.Settings.Default.CheckedUpnpSave = checkUpnp.Checked;
            Properties.Settings.Default.Save();
            parentForm.loadUpnp();
        }

        private void comboWeather_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < comboWeather.Items.Count; i++)
                {
                    if (comboWeather.SelectedIndex == i)
                    {
                        Properties.Settings.Default.ComboWeatherIcon =(@Application.StartupPath + "/pictures/icons" + i);
                        Properties.Settings.Default.ComboWeatherID = comboWeather.SelectedIndex;
                        Properties.Settings.Default.Save();
                        parentForm.getWeather();
                    }
                }
            }           
            catch (Exception ex)
            {
                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[PROBLEM VISIBLE ICONS WEATHER]");
                parentForm.textError = ex.ToString();
                parentForm.textErrorMessage = ex.Message.ToString();
                parentForm.error();
            }
        }

        private void comboBoxWeather_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < comboBoxWeather.Items.Count; i++)
                {
                    if (comboBoxWeather.SelectedIndex == i)
                    {
                        Properties.Settings.Default.CheckedWeatherCity = comboBoxWeather.SelectedIndex;
                        Properties.Settings.Default.Save();
                        parentForm.getWeather();
                    }
                }
            }
            catch (Exception ex)
            {

                applog.write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                applog.write("[PROBLEM WEATHER CITY]");
                parentForm.textError = ex.ToString();
                parentForm.textErrorMessage = ex.Message.ToString();
                parentForm.error();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialogForm.ShowDialog() == DialogResult.OK)
            {
                labelColorForm.BackColor = colorDialogForm.Color;
                Properties.Settings.Default.ColorForm = colorDialogForm.Color;
                Properties.Settings.Default.Save();
                parentForm.setSizeWidget();
            }
            else labelColorForm.BackColor = Properties.Settings.Default.ColorForm;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (colorDialogForm.ShowDialog() == DialogResult.OK)
            {
                labelColorText.BackColor = colorDialogForm.Color;
                Properties.Settings.Default.ColorText = colorDialogForm.Color;
                Properties.Settings.Default.Save();
                parentForm.setSizeWidget();
            }
            else labelColorText.BackColor = Properties.Settings.Default.ColorText;

        }

        private void buttonDropping_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ColorForm = Color.FromArgb(46, 49, 115);
            Properties.Settings.Default.ColorText = Color.FromArgb(255,255,255);
            Properties.Settings.Default.Save();
            parentForm.setSizeWidget();            
        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string url = "https://ta.uilim.ru/api/update";
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string response;
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    response = streamReader.ReadToEnd();
                }
                ApiResponse apiResponse = JsonConvert.DeserializeObject<ApiResponse>(response);


                string str;
                str = apiResponse.update.version.ToString();
                int newVersion;
                newVersion = Convert.ToInt32(str);

                //Моя версия
                String strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                

                strVersion = strVersion.Replace(".", string.Empty);
                int myVersion = Convert.ToInt32(strVersion);

                if (myVersion < newVersion)
                {
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = @Application.StartupPath + "/Updater.exe";
                    proc.StartInfo.WorkingDirectory = @Application.StartupPath;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.Start();
                }
                else
                {
                    MessageBox.Show("Программа не нуждается в обновлении.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                parentForm.textError = ex.ToString();
                parentForm.textErrorMessage = ex.Message.ToString();
                parentForm.error();
            }
        }

    }
}
