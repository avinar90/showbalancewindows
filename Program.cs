﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Principal;
using System.ComponentModel;
using System.Diagnostics;

namespace ITK
{
    static class Program
    {
        private static Mutex m_instance;
        private const string m_appName = "ShowBalance";

        
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool tryCreateNewApp;
            m_instance = new Mutex(true, m_appName, out tryCreateNewApp);
            if (tryCreateNewApp)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new fMain());
                getInfoUpdate();
            }
            else
                MessageBox.Show("Приложение уже запущено", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);               

        }


        //static void Main()
        //{
        //    WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
        //    bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);

        //    if (hasAdministrativeRight == false)
        //    {
        //        ProcessStartInfo processInfo = new ProcessStartInfo(); //создаем новый процесс
        //                                                               //processInfo.Verb = "ITK.exe"; //в данном случае указываем, что процесс должен быть запущен с правами администратора
        //                                                               // processInfo.FileName = Application.ExecutablePath; //указываем исполняемый файл (программу) для запуска  @Application.StartupPath

        //        processInfo.FileName = (@Application.StartupPath + "/ITK.exe");
        //        try
        //        {
        //            Process.Start(processInfo); //пытаемся запустить процесс
        //        }
        //        catch (Win32Exception)
        //        {
        //            //Ничего не делаем, потому что пользователь, возможно, нажал кнопку "Нет" в ответ на вопрос о запуске программы в окне предупреждения UAC (для Windows 7)
        //        }
        //        Application.Exit(); //закрываем текущую копию программы (в любом случае, даже если пользователь отменил запуск с правами администратора в окне UAC)
        //    }
        //    else //имеем права администратора, значит, стартуем
        //    {
        //        bool tryCreateNewApp;
        //        m_instance = new Mutex(true, m_appName,
        //                out tryCreateNewApp);
        //        if (tryCreateNewApp)
        //        {
        //            Application.EnableVisualStyles();
        //            Application.SetCompatibleTextRenderingDefault(false);
        //            getInfoUpdate();
        //        }
        //        else
        //            MessageBox.Show("Приложение уже запущено", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //    }
        //}


        static void getInfoUpdate()
        {
            try
            {
                string url = "http://91.219.137.30/api/update";
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string response;
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    response = streamReader.ReadToEnd();
                }
                ApiResponse apiResponse = JsonConvert.DeserializeObject<ApiResponse>(response);


                string str;
                str = apiResponse.update.version.ToString();
                int newVersion;
                newVersion = Convert.ToInt32(str);

                //Моя версия
                String strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                strVersion = strVersion.Replace(".", string.Empty);
                int myVersion = Convert.ToInt32(strVersion);

                if (myVersion < newVersion)
                {
                    try
                    {
                        System.Diagnostics.Process proc = new System.Diagnostics.Process();
                        proc.StartInfo.FileName = @Application.StartupPath + "/Updater.exe";
                        proc.StartInfo.WorkingDirectory = @Application.StartupPath;
                        proc.StartInfo.CreateNoWindow = true;
                        proc.Start();
                    }
                    catch
                    {
                        Application.Run(new fMain());
                    }

                }
                else
                {
                    Application.Run(new fMain());
                }
            }
            catch
            {
                //MessageBox.Show("Error");
                Application.Run(new fMain());
            }
        }


    }    
}
