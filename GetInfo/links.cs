﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITK
{
    class Links
    {
        public string id { get; set; }

        public string icon { get; set; }

        public string name { get; set; }

        public string url { get; set; }

    }
}
