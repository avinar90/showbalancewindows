﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ITK
{
    class Weather
    {
        public string icon;

        public string name;

        public Bitmap Icon
        {
            get
            {
                return new Bitmap(Image.FromFile((Properties.Settings.Default.ComboWeatherIcon) + $"/{icon}.png"));
            }
        }


        private double _temp;
        public double temp
        {
            get
            {
                return _temp;
            }
            set
            {
                _temp = value - 273.15; //celci9
            }
        }

        //public string icon { get; set; }

        //public string name { get; set; }

        //public string temp { get; set; }
    }
}
