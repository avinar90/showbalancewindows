﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITK
{
    class ApiResponse
    {
        public ApiUpdate update { get; set; }
        public List<Links> links { get; set; }

        // public List<Weather> weather { get; set; }
        public Weather[] Weather;
    }
}
