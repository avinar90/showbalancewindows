﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITK
{
    class log
    {
        //конструктор
        public log()
        {
        }        

        public void write(string msg)
        {
            filesize();
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"log.log", true))
                {
                    file.WriteLine(String.Format("{0:dd.MM.yy HH:mm:ss} {1}", DateTime.Now, msg));
                    file.Close();
                }
            }
            catch
            {
            }
        }

        /**
         * 
         * 
         */
        public void filesize()
        {
            try
            {
                if (((new FileInfo(@"log.log").Length) / 1024 / 1024) > 5) clear();
            }
            catch
            {
            }
        }

        public void clear()
        {
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"log.log", false))
                {
                    file.Close();
                }
            }
            catch
            {
            }
        }
    }
}
